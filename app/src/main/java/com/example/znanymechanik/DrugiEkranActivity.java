package com.example.znanymechanik;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import static com.example.znanymechanik.R.id.button_blacharstwo;
import static com.example.znanymechanik.R.id.button_lakiernictwo;
import static com.example.znanymechanik.R.id.button_pickUp;
import static com.example.znanymechanik.R.id.button_sedan;
import static com.example.znanymechanik.R.id.button_suv;
import static com.example.znanymechanik.R.id.button_van;

public class DrugiEkranActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drugi_ekran);
    }


    public void Click_TrzeciEkran(View view) {
        switch (view.getId())
        {
            case button_suv:
            case button_van:
            case button_pickUp:
            case button_sedan:
            {
                Button b =(Button) findViewById(button_suv);
                View.OnClickListener l = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri uri = Uri.parse("http://pp57000.wsbpoz.solidhost.pl/test.php");
                        Intent i = new Intent(Intent.ACTION_VIEW,uri);
                        startActivity(i);
                    }
                } ;
                b.setOnClickListener(l);
            }



                //Intent intent = new Intent(DrugiEkranActivity.this,  TrzeciEkran.class);
                //startActivity(intent);

                break;
        }
    }
}